#!/bin/env sh

tgSerPre=https://api.telegram.org/bot
token=${1}
delay=1
updateid=0

process () {
    tmp=$(echo ${3} | sed -e 's/🖤/ /g')
    sendmsg=$(${tmp}) #XDDD in fact this is the core of the program

    oldIFS=${IFS} && IFS=":"
    printf "[send_text]:%s\n" "${sendmsg}"
    IFS=${oldIFS} && unset oldIFS
    
    if [ -z "${sendmsg}" ] ; then
	printf "[warinning]:empty send message!\n"
    else
	curl -s\
	     --data-urlencode "chat_id=${1}"\
	     --data-urlencode "text=${sendmsg}"\
	     "${tgSerPre}${token}/sendMessage"
    fi
}

poll () {
    curl -s "${tgSerPre}${token}/getUpdates?offset=${updateid}" | sed -e 's/ /🖤/g' >updates

    updateid=$(grep -o '"result":[{"update_id":[0-9]\+' updates | grep -o '[0-9]\+')

    printf "\n###################################\n"
    printf "[updateid]:%s\n" ${updateid:-empty}
    printf "###################################\n\n"
    
    if [ -z "${updateid}" ] ; then
	return 1
    fi
        
    list=$(grep -o '"message":{.\+' updates)
    printf "%s\n" ${list}

    for item in ${list} ;
    do
	#printf "%s" ${item}
	chatid=$(echo ${item} | grep -o '"chat":{"id":[0-9]\+' | grep -o '[0-9]\+')
	printf "[chat_id]:%s\n" ${chatid}
	
	msgid=$(echo ${item} | grep -o ':{"message_id":[0-9]\+' | grep -o '[0-9]\+')
	printf "[message_id]:%s\n" ${msgid}
	
	text=$(echo ${item} | grep -o '"text":".\+"' | cut -d '"' -f 4)
	printf "[receive_text]:%s\n" ${text}

	updateid=$((${updateid} + 1))
	
	process ${chatid} ${msgid} ${text}
    done
}

while true ;
do
    poll
    sleep ${delay}
done
